
year = int(input("Please enter a year: \n"))
if year <= 0 :
    print("Invalid year")
elif  year % 4 == 0:
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")


rows = int(input("Enter number of rows: \n"))
cols = int(input("Enter number of cols: \n"))

i = 1
while(i <= rows):
    j = i 
    while(j < cols +1):
        print("*",end='')
        j += 1
    k = 1 
    while(k < i ):
        print("*",end='')
        k += 1
    print()
    i = i + 1