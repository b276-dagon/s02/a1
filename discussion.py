# Python also allows user to input, with this, users can give inputs to the program

# [Section] input
# username = input("Please enter your name: ")
# print(f"Hello {username}! Welcome to python short course!")


# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))
# print(f"The sum of num1 and num2 is {num1+num2}")

# [Section] With user inputs, users can give inputs for the program to be used to control the application using control structures.
# Control structures can be divided into selection and repitition control structures

# Selection control structures allows the program to choose among choices and run specific codes dependeing on the choice taken(conditions)

# Repitition Control Structures allow the program to repeat certain blocks of code given a starting condition and termination condition

# [Section] If-else statements
# if-else statements are used to choose between two or more code blocks depending on the condition

test_num = 75

if test_num >= 60 :
	print("Test passed!")
else:
	print("Test failed!")

trial = 55 > 40
print(trial)

# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed.

# [Section] if else chains can also be used to have more than 2 choices for the program 

# test_num2 = int(input("Please enter the second number\n"))

# if test_num2 > 0 :
# 	print("The number is positive!")
# 	print(f"The number you provided is {test_num2}!")
# elif test_num2 == 0 :
# 	print("The number is equal to 0!")
# else:
# 	print("The number is negative!")

# Mini Exercise 1:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both. 
# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number is not divisible by 3 nor 5"


# number = int(input("Please Enter Number: \n"))

# if number % 3 == 0 and number % 5 == 0 :
#     print("The number is divisivle by 3 and 5")
# elif number % 3 == 0 :
#     print("The number is divisivle by 3")
# elif number % 5 == 0 :
#     print("The number is divisivle by 5")
# else:
#     print("The number is not divisible by 3 nor 5")


# [Section] Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statements as long as the condition is true

i = 0

# while i <= 5 :
# 	i += 2
# 	print(f"Current value of i is {i}")


# [Section] for loops are used for iteration over a sequence

fruits = ["apple", "banana", "cherry"] #lists

for fruit in fruits:
	print(fruit)

# [Section] range() method
# To use the for loop to iterate through values, the range method can be used

# default starting value is 0
# for x in range(6):
# 	print(f'The current value of x is {x}!')

# for x in range(6, 10):
# 	print(f'The current value of x is {x}!')

for x in range(6, 20, 2):
	print(f'The current value of x is {x}!')

# [Section] Break Statement
#The break statement is used to stop the loop

# j = 1
# while j < 6 :
# 	print(j)
	
# 	if j==3:
# 		break

# 	j+=1

# [Section] Continue Statement
# The continue statement it returns the control to the beginning of the while loop and continue to the next iteration

k = 1

while k < 6 :
	
	
	if k==3:
		k += 1
		continue

	k += 1
	print(k)
